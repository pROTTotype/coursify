package de.swe.coursify;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MenuProfActivity extends Activity implements OnClickListener {
	private Button button1, button2, button3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_prof);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		button1 = (Button) findViewById(R.id.button1);
		button1.setOnClickListener(this);

		button2 = (Button) findViewById(R.id.button2);
		button2.setOnClickListener(this);

		button3 = (Button) findViewById(R.id.button3);
		button3.setOnClickListener(this);

		return true;
	}

	@Override
	public void onClick(View arg0) {
		int ce = arg0.getId();

		switch (ce) {
		case R.id.button1:
			Intent evaluation = new Intent(MenuProfActivity.this, EvaluationActivity.class);
			startActivity(evaluation);
			break;
		case R.id.button2:
			finish();
			break;
		case R.id.button3:
			Intent lecture = new Intent(MenuProfActivity.this, LectureActivity.class);
			startActivity(lecture);
			break;
		default:
			break;
		}

	}

}
