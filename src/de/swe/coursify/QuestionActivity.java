package de.swe.coursify;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class QuestionActivity extends Activity implements OnClickListener {

	private Button submit, next, prev;
	private ImageView prepic;
	private int i = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.questions_presentation);
		// prepic.setOnLongClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		prepic = (ImageView) findViewById(R.id.prespic);
		final AlertDialog.Builder alert = new AlertDialog.Builder(QuestionActivity.this);
		alert.setTitle("Seite auswaehlen");
		alert.setMessage("Bitte gewuenschte Seite eintippen: ");
		final EditText input = new EditText(this);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				i = Integer.parseInt(input.getText().toString());
				navigate();
			}
		});

		alert.setView(input);

		prepic.setOnLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				alert.show();
				return false;
			}
		});

		prepic.setBackgroundResource(R.drawable.presentation_0);

		next = (Button) findViewById(R.id.next);
		next.setOnClickListener(this);
		prev = (Button) findViewById(R.id.prev);
		prev.setOnClickListener(this);

		submit = (Button) findViewById(R.id.submit);
		submit.setOnClickListener(this);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onClick(View arg0) {

		int ce = arg0.getId();

		switch (ce) {
		case R.id.next:
			i++;
			navigate();
			break;
		case R.id.prev:
			i--;
			navigate();
			break;
		case R.id.submit:
			finish();
			break;
		default:
			break;
		}

	}

	private void navigate() {
		switch (i) {
		case 0:
			prepic.setBackgroundResource(R.drawable.presentation_0);
			break;
		case 1:
			prepic.setBackgroundResource(R.drawable.presentation_1);
			break;
		case 2:
			prepic.setBackgroundResource(R.drawable.presentation_2);
			break;
		case 3:
			prepic.setBackgroundResource(R.drawable.presentation_3);
			break;
		case 4:
			prepic.setBackgroundResource(R.drawable.presentation_4);
			break;
		case 5:
			prepic.setBackgroundResource(R.drawable.presentation_5);
			break;
		case 6:
			prepic.setBackgroundResource(R.drawable.presentation_6);
			break;
		case 7:
			prepic.setBackgroundResource(R.drawable.presentation_7);
			break;
		default:
			i = 0;
			prepic.setBackgroundResource(R.drawable.presentation_0);
			break;
		}
	}

}
