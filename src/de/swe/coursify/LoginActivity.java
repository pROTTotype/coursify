package de.swe.coursify;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends Activity implements OnClickListener {
	private Button submit;
	private TextView User_Textbox, Password_Textbox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_view);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		submit = (Button) findViewById(R.id.buttonlogin);
		submit.setOnClickListener(this);

		User_Textbox = (TextView) findViewById(R.id.editTextUser);
		Password_Textbox = (TextView) findViewById(R.id.editTextPassword);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onClick(View arg0) {
		String Prof_USR = "prof";
		String Prof_PWD = "prof";
		String Tutor_USR = "tutor";
		String Tutor_PWD = "tutor";
		String Student_USR = "student";
		String Student_PWD = "student";

		String User_Textbox_Text = (String) User_Textbox.getText().toString();
		String Password_Textbox_Text = (String) Password_Textbox.getText().toString();

		if (arg0.getId() == R.id.buttonlogin) {
			if (User_Textbox_Text.equals(Prof_USR) && Password_Textbox_Text.equals(Prof_PWD)) {
				Intent menu = new Intent(this, MenuProfActivity.class);
				startActivity(menu);

			} else if (User_Textbox_Text.equals(Tutor_USR) && Password_Textbox_Text.equals(Tutor_PWD)) {
				finish();
			} else if (User_Textbox_Text.equals(Student_USR) && Password_Textbox_Text.equals(Student_PWD)) {
				Intent menu = new Intent(this, MenuStudentActivity.class);
				startActivity(menu);
			} else {
				AlertDialog alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setTitle("Fehler");
				alertDialog.setMessage("Authentifikation fehlgeschlagen");
				alertDialog.show();
			}
		}
	}

}
