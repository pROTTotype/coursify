package de.swe.coursify;

import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LectureActivity extends Activity implements OnClickListener {
	Button submit;
	TextView datetext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lecture);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		submit = (Button) findViewById(R.id.button9);
		submit.setOnClickListener(this);
		
		Date date = new Date();
		datetext = (TextView) findViewById(R.id.textView3);
		String text = (String)datetext.getText() + ' ' + date;
		datetext.setText(text);
		
		return true;
	}

	@Override
	public void onClick(View arg0) {
		finish();
	}
}
